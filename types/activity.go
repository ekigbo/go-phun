package types

import (
	"strings"
	"time"
)

type ActivityType int

const (
	ACTIVITY_LIGHT ActivityType = iota
	ACTIVITY_RUN
	ACTIVITY_CYCLING
)

type ActivityMetadata struct {
	RecordID       string
	Name           string
	Description    string
	Timestamp      time.Time
	TrainingEffect float32
	Duration       time.Duration
	Calories       int
}

type Activity struct {
	Type                               ActivityType
	TypeName                           string // string representation of the activity type, map to strava
	Metadata                           ActivityMetadata
	Distance                           float32
	HRMin, HRMax, HRAvg                int
	CadenceMin, CadenceMax, CadenceAvg int
	SpeedMax, SpeedAvg                 float32
	PaceMax, PaceAvg                   float32
	Steps                              int
	Stride                             int
	VO2MAX                             int
	HRRangeExtremeDuration             time.Duration
	HRRangeAnaerobicDuration           time.Duration
	HRRangeAerobicDuration             time.Duration
	HRRangeFatBurningDuration          time.Duration
	HRRangeWarmUpDuration              time.Duration
}

// Functions

// Use the options pattern for building different activities
// https://golang.cafe/blog/golang-functional-options-pattern.html
// https://github.com/tmrts/go-patterns/blob/master/idiom/functional-options.md
type ConfigOption func(*Activity)

func Speed(max, avg float32) ConfigOption {
	return func(args *Activity) {
		args.SpeedMax = max
		args.SpeedAvg = avg
	}
}

func Pace(avg, max float32) ConfigOption {
	return func(args *Activity) {
		args.PaceMax = max
		args.PaceAvg = avg
	}
}

func Cadence(avg, min, max int) ConfigOption {
	return func(args *Activity) {
		args.CadenceMin = min
		args.CadenceMax = max
		args.CadenceAvg = avg
	}
}

func Distance(distance float32) ConfigOption {
	return func(args *Activity) {
		args.Distance = distance
	}
}

func StepsAndStride(steps, stride int) ConfigOption {
	return func(args *Activity) {
		args.Steps = steps
		args.Stride = stride
	}
}

func VO2MAX(value int) ConfigOption {
	return func(args *Activity) {
		args.VO2MAX = value
	}
}

func HR(avg, min, max int) ConfigOption {
	return func(args *Activity) {
		args.HRMin = min
		args.HRMax = max
		args.HRAvg = avg
	}
}

type hRRange struct {
	HRRangeExtremeDuration    int
	HRRangeAnaerobicDuration  int
	HRRangeAerobicDuration    int
	HRRangeFatBurningDuration int
	HRRangeWarmUpDuration     int
}

func HRRange(hrr hRRange) ConfigOption {
	return func(args *Activity) {
		args.HRRangeExtremeDuration = toDurationSeconds(hrr.HRRangeExtremeDuration)
		args.HRRangeAnaerobicDuration = toDurationSeconds(hrr.HRRangeAnaerobicDuration)
		args.HRRangeAerobicDuration = toDurationSeconds(hrr.HRRangeAerobicDuration)
		args.HRRangeFatBurningDuration = toDurationSeconds(hrr.HRRangeFatBurningDuration)
		args.HRRangeWarmUpDuration = toDurationSeconds(hrr.HRRangeWarmUpDuration)
	}
}

// Generates a name based on some of the metadata
func generateName(t ActivityType, duration time.Duration, timestamp time.Time) string {
	// 0 - 35mins short
	// 40 - 65mins
	// 65+mins - long
	// Before midday = morning
	// Before 3pm = afternoon
	// After 3pm = evening
	// ie. Manual Entry - Short morning light exercise
	return ""
}

func toDurationSeconds(v int) time.Duration {
	return time.Duration(v) * time.Second
}

func NewActivityFromExerciseRecord(er *ExerciseRecord) Activity {
	var a *Activity

	t := ResolveActivityType(er.Fields.Type)
	duration := toDurationSeconds(er.Fields.DurationSeconds)
	am := ActivityMetadata{
		RecordID:       er.ID,
		Timestamp:      er.Fields.Timestamp,
		TrainingEffect: er.Fields.TrainingEffect,
		Calories:       er.Fields.Calories,
		Duration:       duration,
		Name:           generateName(t, duration, er.Fields.Timestamp),
	}

	a = &Activity{Type: t, Metadata: am}
	erf := &er.Fields

	a.applySetters(
		HR(erf.HRMin, erf.HRMax, erf.HRAvg),
		HRRange(hRRange{
			HRRangeExtremeDuration:    erf.HRRangeExtremeDuration,
			HRRangeAnaerobicDuration:  erf.HRRangeAnaerobicDuration,
			HRRangeAerobicDuration:    erf.HRRangeAerobicDuration,
			HRRangeFatBurningDuration: erf.HRRangeFatBurningDuration,
			HRRangeWarmUpDuration:     erf.HRRangeWarmUpDuration,
		}),
	)

	switch t {
	case ACTIVITY_CYCLING:
		a = NewCyclingEvent(a, erf)
	case ACTIVITY_RUN:
		a = NewRunEvent(a, erf)
	}
	return *a
}

func NewCyclingEvent(a *Activity, erf *ExerciseRecordFields) *Activity {
	return a.applySetters(
		VO2MAX(erf.VO2MAX),
		Distance(erf.Distance),
		Speed(erf.SpeedAvg, erf.SpeedMax),
		Pace(erf.PaceAvg, erf.PaceMax),
		Cadence(erf.CadenceAvg, erf.CadenceMin, erf.CadenceMax),
	)
}

func NewRunEvent(a *Activity, erf *ExerciseRecordFields) *Activity {
	return a.applySetters(
		VO2MAX(erf.VO2MAX),
		Distance(erf.Distance),
		StepsAndStride(erf.Steps, erf.Stride),
		Speed(erf.SpeedAvg, erf.SpeedMax),
		Pace(erf.PaceAvg, erf.PaceMax),
		Cadence(erf.CadenceAvg, erf.CadenceMin, erf.CadenceMax),
	)
}

func ResolveActivityType(s string) ActivityType {
	switch strings.ToLower(s) {
	case "cycling":
		return ACTIVITY_CYCLING
	case "run":
		return ACTIVITY_RUN
	default:
		return ACTIVITY_LIGHT
	}
}

// Methods
func (a *Activity) applySetters(setters ...ConfigOption) *Activity {
	for _, setter := range setters {
		setter(a)
	}
	return a
}

// func (a *Activity) toString() string {
// 	return fmt.Sprintf("")
// }
