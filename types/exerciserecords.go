package types

import "time"

type ExerciseRecordFields struct {
	Timestamp time.Time `json:"Date"`
	Type      string    `json:"Type"`
	// Duration is time in seconds
	DurationSeconds           int     `json:"Duration"`
	Distance                  float32 `json:"Distance (km)"`
	TrainingEffect            float32 `json:"TE"`
	Calories                  int     `json:"Calories (kcal)"`
	HRMin                     int     `json:"HR Min"`
	HRMax                     int     `json:"HR Max"`
	HRAvg                     int     `json:"Heart rate Avg (bpm)"`
	HRRangeExtremeDuration    int     `json:"HR Extreme"`     // measured in seconds
	HRRangeAnaerobicDuration  int     `json:"HR Anaerobic"`   // measured in seconds
	HRRangeAerobicDuration    int     `json:"HR Aerobic"`     // measured in seconds
	HRRangeFatBurningDuration int     `json:"HR Fat burning"` // measured in seconds
	HRRangeWarmUpDuration     int     `json:"HR Warm up"`     // measured in seconds
	// Cadence values here relate to steps / min
	CadenceAvg int     `json:"Cadence (steps/min)"`
	CadenceMax int     `json:"Cadence max"`
	CadenceMin int     `json:"Cadence min"`
	Steps      int     `json:"Steps"`
	Stride     int     `json:"Stride (cm)"`
	PaceMax    float32 `json:"Pace max"`
	PaceAvg    float32 `json:"Avg pace (mins/km)"`
	SpeedMax   float32 `json:"Max Speed (km/h)"`
	SpeedAvg   float32 `json:"Avg Speed"`
	VO2MAX     int     `json:"VO2max (ml/kg/min)"`
}

type ExerciseRecord struct {
	ID          string               `json:"id"`
	CreatedTime string               `json:"createdTime"`
	Fields      ExerciseRecordFields `json:"fields"`
}
