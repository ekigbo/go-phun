package exercisedata

import (
	"fmt"
	"log"
	"os"
	"path"

	"github.com/joho/godotenv"
	"gitlab.com/ekigbo/go-phun/airtable"
	"gitlab.com/ekigbo/go-phun/types"
)

const (
	ENV_AIRTABLE_KEY      = "AIRTABLE_KEY"
	ENV_AIRTABLE_APP_ID   = "AIRTABLE_APP_ID"
	ENV_AIRTABLE_TABLE_ID = "AIRTABLE_TABLE_ID"
)

type Config struct {
	Airtable airtable.Config
	// Strava   ApiCredentials
}

func fetchAirtableData() {
	// makes a http request to our raw data
}

func GetConfig() *Config {
	return &Config{
		Airtable: airtable.Config{
			TableID: os.Getenv(ENV_AIRTABLE_TABLE_ID),
			Key:     os.Getenv(ENV_AIRTABLE_KEY),
			AppID:   os.Getenv(ENV_AIRTABLE_APP_ID),
		},
	}
}

// Formatted output to the command line with gauges / nice metrics
// Takes an array of activities and prints them to the command line
func prettyPrint([]types.Activity) {}

func FetchAllActivities(c *Config) {
	fmt.Println("Fetching all airtable activities")

	ar := airtable.NewRequest(c.Airtable, 3)
	records := airtable.GetRecords(&ar)

	var activities []types.Activity
	for _, r := range records {
		a := types.NewActivityFromExerciseRecord(&r)
		fmt.Printf("\n%+v\n", a)
		activities = append(activities, a)
	}

}

func init() {
	cwd, pathErr := os.Getwd()
	if pathErr != nil {
		log.Fatal(pathErr)
	}

	envfilePath := path.Join(cwd, "./exercisedata/.env")
	err := godotenv.Load(envfilePath)
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}
