package main

import (
	"fmt"

	"gitlab.com/ekigbo/go-phun/exercisedata"
)

func main() {
	fmt.Println("Welcome, Wilkommen, Bienvenue!")

	c := exercisedata.GetConfig()
	exercisedata.FetchAllActivities(c)
}
