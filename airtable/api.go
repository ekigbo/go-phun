package airtable

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/ekigbo/go-phun/types"
)

// TODO: extract this to a separate module for reuse with generics
type Config struct {
	TableID, Key, AppID string
}

type RequestSort struct {
	Field, Direction string
}

type Request struct {
	Config
	MaxRecords    int
	SortField     string
	SortDirection string
	Sort          []RequestSort
}

type ApiResponseExerciseRecord struct {
	ExerciseRecords []types.ExerciseRecord `json:"records"`
}

func handleError(err error) {
	// TODO: need a logger / handler
	if err != nil {
		log.Fatal(err)
	}
}

func baseString(r *Request, endpoint string) string {
	return fmt.Sprintf("https://api.airtable.com/v0/%s/%s", r.AppID, r.TableID)
}

func getRequest(r *Request, endpoint string) []byte {
	url := baseString(r, endpoint)
	// fmt.Println("Request URL", url)
	client := &http.Client{}

	reqConfig, err := http.NewRequest(http.MethodGet, url, nil)
	handleError(err)

	// set headers
	reqConfig.Header.Set("Authorization", fmt.Sprintf("Bearer %s", r.Key))

	// set query parameters
	values := reqConfig.URL.Query()
	values.Add("maxRecords", fmt.Sprint(r.MaxRecords))

	for index, s := range r.Sort {
		values.Add(fmt.Sprintf("sort[%d][field]", index), s.Field)
		values.Add(fmt.Sprintf("sort[%d][direction]", index), s.Direction)
	}

	reqConfig.URL.RawQuery = values.Encode()

	// fmt.Printf("\n%+v\n", reqConfig)

	resp, reqErr := client.Do(reqConfig)
	handleError(reqErr)

	defer resp.Body.Close()

	byteData, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	return byteData
}

// TODO: should probably use generics here for fetch different types of records
func GetRecords(ar *Request) []types.ExerciseRecord {
	data := getRequest(ar, "")

	r := ApiResponseExerciseRecord{} // I think would be our generic type thats returned
	err := json.Unmarshal(data, &r)  // Unmarshalling should be handled by the caller
	handleError(err)

	// fmt.Printf("\napi response\n")
	// for _, record := range r.ExerciseRecords {
	// 	fmt.Printf("\nrecord: \n%+v\n", record)
	// }

	return r.ExerciseRecords
}

func NewRequest(c Config, maxRecords int) Request {
	return Request{
		Config:     c,
		MaxRecords: maxRecords,
		Sort: []RequestSort{
			{Field: "Date", Direction: "desc"},
			{Field: "Duration", Direction: "desc"},
		},
	}
}
